import colors from './Colors.js';
import analyzerHelper from './AnalyzerHelper.js';

export default {
analyzeText : function(text) {
    let names = ["שירה", "אורי", "דנה", "דן", "דניאל", "מיטל", "מוריה", "תום", "ליה", "מאיה", "אורן", "אלון","ענת","יעל","גיא","תומר","הילה","מתן","רועי","עומר","יובל"];
    let todo = [];
    todo.splice(0,todo.length);
    let descArray = analyzerHelper.seperateTokens(text);
    let i = 0;
    for (i=0;i<descArray.length;i++) {
        let desc = descArray[i];
        if (desc.includes("עיניים") || desc.includes("עינים")) {

            let newAttachment = "black";

            if (desc.includes("ירוק")) {
                newAttachment = "green";
            } else if (desc.includes("כחול")) {
                newAttachment = "blue";
            } else if (desc.includes("חומות") || desc.includes("חום")) {
                newAttachment = "brown";
            } 

            todo = analyzerHelper.addChangeTodo("eyes", newAttachment, todo);
            
        } else if (desc.includes("שיער") || desc.includes("שער")) {
            
            let tint = 0xFFFFFF;
            let longAttachment = undefined;
            if (desc.includes("חום")) {
                tint = colors.BROWN_HAIR;
            } else if (desc.includes("שחור")) {
                tint = colors.BLACK;
            } else if (desc.includes("בלונדיני")) {
                tint = colors.BLOND_HAIR;
            } else if (desc.includes("ג'ינג'י")) {
                tint = colors.GINGER_HAIR;
            } else {
                tint = colors.colorForWords(desc);
            }
            
            let shortAttachment = "straight";
            if (descArray[i+1] && descArray[i+1].includes("קוקו")) {
                shortAttachment = "ponytail";
                if (descArray[i+1].includes("חום")) {
                    tint = colors.BROWN_HAIR;
                } else if (descArray[i+1].includes("שחור")) {
                    tint = colors.BLACK;
                }if (descArray[i+1].includes("בלונדיני")) {
                    tint = colors.BLOND_HAIR;
                }if (descArray[i+1].includes("ג'ינג'י")) {
                    tint = colors.GINGER_HAIR;
                } else {
                    tint = colors.colorForWords(descArray[i+1]);
                }   
                descArray[i+1] = "";
            } else if (desc.includes("ארוך")) {
                
                shortAttachment = "long prep";
                
                longAttachment = "straight";
                
                if (desc.includes("מתולתל") || desc.includes("מתלתל")) {
                    longAttachment = "curly";
                }
                
            } else if (desc.includes("מתולתל") || desc.includes("מתלתל")) {
                console.log("curly");
                shortAttachment = "curly";
            }
            
            todo = analyzerHelper.addChangeTodo("short hair",shortAttachment, todo);
            todo = analyzerHelper.addTintTodo("short hair", tint,todo);
            
             if (longAttachment) {
                todo = analyzerHelper.addChangeTodo("long hair", longAttachment, todo);
                todo = analyzerHelper.addTintTodo("long hair", tint, todo);
            }
            
        } else if (desc.includes("קוקו")) {
            todo = analyzerHelper.addChangeTodo("short hair", "ponytail", todo);
            todo = analyzerHelper.addChangeTodo("long hair", "none", todo);
        
            let tint = 0xFFFFFF;
        
            if (desc.includes("חום")) {
                tint = colors.BROWN_HAIR;
            } else if (desc.includes("שחור")) {
                tint = colors.BLACK;
            } else if (desc.includes("בלונדיני")) {
                tint = colors.BLOND_HAIR;
            } else if (desc.includes("ג'ינג'י")) {
                tint = colors.GINGER_HAIR;
            } else {
                tint = colors.colorForWords(desc);
            } 
        
            if (tint > -1) {
                todo = analyzerHelper.addTintTodo("short hair",tint, todo);
            }
        } else if (desc.includes("חולצה")) {
            todo = analyzerHelper.addChangeTodo("shirt-dress","shirt", todo);
            let tint = colors.colorForWords(desc);
            console.log(tint);
            todo = analyzerHelper.addTintTodo("shirt-dress", tint, todo);
            
            if (desc.includes("ארוכ")) {
                todo = analyzerHelper.addChangeTodo("long sleeves","sleeve", todo);
                todo = analyzerHelper.addTintTodo("long sleeves", tint, todo);
            }
        } else if (desc.includes("שמלה")) {
            let tint = colors.colorForWords(desc);
            todo = analyzerHelper.addChangeTodo("shirt-dress","dress", todo);
            todo = analyzerHelper.addTintTodo("shirt-dress",tint,todo);
        } else if (desc.includes("מכנסיים") || desc.includes("מכנסים")) {
            let tint = colors.colorForWords(desc);
            todo = analyzerHelper.addTintTodo("pants", tint,todo);
            if (desc.includes("ארוכ")) {
                todo = analyzerHelper.addChangeTodo("long pants", "pant",todo);
                todo = analyzerHelper.addTintTodo("long pants", tint, todo);
            }
        } else if (desc.includes("נעלי")) {
            let tint = colors.colorForWords(desc);
            todo = analyzerHelper.addChangeTodo("shoes","shoe",todo);
            todo = analyzerHelper.addTintTodo("shoes",tint,todo);
        } else if (desc.includes("סנדלים")) {
            let tint = colors.colorForWords(desc);
            todo = analyzerHelper.addChangeTodo("shoes","sandal",todo);
            todo = analyzerHelper.addTintTodo("shoes",tint,todo);
        } else if (desc.includes("כובע")) {
            let tint = colors.colorForWords(desc);
            todo = analyzerHelper.addChangeTodo("hat","hat",todo);
            todo = analyzerHelper.addTintTodo("hat",tint,todo);
        } else if (desc.includes("משקפיים") || desc.includes("משקפים")) {
            let tint = colors.colorForWords(desc);
            todo = analyzerHelper.addChangeTodo("glasses","glasses",todo);
            todo = analyzerHelper.addTintTodo("glasses",tint,todo);
        } else if (desc.includes("פלייסטיישן") || desc.includes("פליסטישן") || desc.includes("פלייסטישן") || desc.includes("פליסטיישן")) {
            todo = analyzerHelper.addChangeTodo("playstation","playstation",todo);
        } else if (desc.includes("לצייר") || desc.includes("לציר")) {
            todo = analyzerHelper.addChangeTodo("paint","paint",todo);
        } else if (desc.includes("לקרוא")) {
            todo = analyzerHelper.addChangeTodo("book","book",todo);
        } else if (desc.includes("כדורגל")) {
            todo = analyzerHelper.addChangeTodo("football","football",todo);
        } else if (desc.includes("כדורסל")) {
            todo = analyzerHelper.addChangeTodo("basketball","basketball",todo);
        } else if (desc.includes("בלט")) {
            todo = analyzerHelper.addChangeTodo("ballet","ballet",todo);
        } else if (desc.includes("לגו")) {
            todo = analyzerHelper.addChangeTodo("lego","lego",todo);
        } else if (desc.includes("חוף")) {
            todo = analyzerHelper.addBackgroundTodo("beach",todo);
        } else if (desc.includes("מגרש")) {
            todo = analyzerHelper.addBackgroundTodo("soccer_field",todo);
        } else if (desc.includes("רחוב")) {
            todo = analyzerHelper.addBackgroundTodo("street",todo);
        } else if (desc.includes("גינה")) {
            todo = analyzerHelper.addBackgroundTodo("park",todo);
        } else if (desc.includes("בית ספר") || desc.includes("בית הספר")) {
            todo = analyzerHelper.addBackgroundTodo("schoolyard",todo);
        }  else if (desc.includes("חדר") || desc.includes("בית")) {
            todo = analyzerHelper.addBackgroundTodo("bedroom",todo);
        } else if (desc.includes("כיתה") || desc.includes("כתה")) {
            todo = analyzerHelper.addBackgroundTodo("classroom",todo);
        } else if (desc.includes("מסיבה") || desc.includes("הופעה")) {
            todo = analyzerHelper.addBackgroundTodo("club",todo);
        } else if (desc.includes('"')) {
            let text = "";
            text= desc.substring(1,desc.length);
            if (descArray[i+1] && descArray[i+1].includes('"')) {
                text = text + " " + descArray[i+1].substring(0,descArray[i+1].indexOf('"'));
                descArray[i+1] = "";
            } else {
                text = text.substring(0,text.indexOf('"'));
            }
            todo = analyzerHelper.addSpeechTodo(text,todo);
        } else if (desc.includes("הולכת") || desc.includes("הולך")) {
            todo = analyzerHelper.addAnimationTodo("Walk",3100,2,todo, 3150);
        } else if (desc.includes("רץ") || desc.includes("רצה")) {
            todo = analyzerHelper.addAnimationTodo("Run",3100,5,todo, 3150);
        } else if (desc.includes("שר") && !(desc.includes("עשר"))) {
            todo = analyzerHelper.addAnimationTodo("Singing",0,0,todo, 5000);
        } else if (desc.includes("רוכב")) {
            todo = analyzerHelper.addAnimationTodo("bycicle",3100,4,todo, 3150);
        } else if (desc.includes("בוכה")) {
            todo = analyzerHelper.addAnimationTodo("Crying",0,0,todo, 2700);
        } else if (desc.includes("רוקד")) {
            let randomNum = Math.random();
            
            if (randomNum < 0.3) {
                todo = analyzerHelper.addAnimationTodo("Dance",3100,0,todo, 3150);
            } else if (randomNum < 0.65) {
                todo = analyzerHelper.addAnimationTodo("Dance 2",0,0,todo, 4200);
            } else {
                todo = analyzerHelper.addAnimationTodo("Dance 3",0,0,todo, 4200);
            }
        } else if (desc.includes("קורא")) {
            todo = analyzerHelper.addAnimationTodo("Reading",0,0,todo, 3500);
        } else if (desc.includes("כותב")) {
            todo = analyzerHelper.addAnimationTodo("writing",0,0,todo,3500);
        } else if (desc.includes("יושב") || desc.includes("מתיישב") || desc.includes("מתישב")) {
            todo = analyzerHelper.addAnimationTodo("Set",0,0,todo, 2500);
        } else if (desc.includes("עומד") || desc.includes("נעמד") || desc.includes("קם") || desc.includes("קמה")) {
            todo = analyzerHelper.addAnimationTodo("Stand",0,0,todo, 2500);
        } else {
            names.forEach(function(item,index){
                if (desc.includes(item)) {
                    todo = analyzerHelper.addCharacterTodo(item, todo);
                }
            });
        }
    }

    return todo;
}
}



