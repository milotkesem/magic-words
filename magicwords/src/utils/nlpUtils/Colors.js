

export default {
colorForWords : function(str) {
    console.log(str);
    var i = 0;;
    if (str.includes("כחול") || str.includes("blue")) {
        if (str.includes("כה") || str.includes("dark")){
            return this.DARK_BLUE;
        } else if (str.includes("בהיר") || str.includes("light")) {
            return this.LIGHT_BLUE;
        } else {
            return this.BLUE;
        }
    } else if (str.includes("תכלת")) {
        return this.LIGHT_BLUE;
    }else if (str.includes("ורוד") || str.includes("pink")) {
        return this.PINK;
    } else if (str.includes("טורקיז") || str.includes("tourquoise")) {
        return this.TOURQUOISE;
    } else if (str.includes("ירוק") || str.includes("green")) {
        if (str.includes("כה") || str.includes("dark")){
            return this.DARK_GREEN;
        } else if (str.includes("בהיר") || str.includes("light")) {
            return this.LIGHT_GREEN;
        } else {
            return this.GREEN;
        }
    } else if (str.includes("אדו") || str.includes("red")) {
        return this.RED;
    } else if (str.includes("בורדו") || str.includes("bordaux")) {
        return this.BORDO;
    } else if (str.includes("לב") || str.includes("white")) {
        return this.WHITE;
    } else if (str.includes("שחור") || str.includes("black")) {
        return this.BLACK;
    } else if (str.includes("צהוב") || str.includes("yellow")) {
       return this.YELLOW;
    } else if (str.includes("סגול") || str.includes("purple")) {
        if (str.includes("בהיר") || str.includes("light")) {
            return this.LIGHT_PURPLE;
        } else if (str.includes("כה") || str.includes("dark")) {
            return this.DARK_PURPLE;
        } else {
            return this.PURPLE;
        }
    } else if (str.includes("כתו") || str.includes("orange")) {
       return this.ORANGE;
    } else if (str.includes("חום") || str.includes("חומ") || str.includes("brown")) {
        if (str.includes("כה") || str.includes("dark")){
            return this.DARK_BROWN;
        } else if (str.includes("בהיר") || str.includes("light")) {
            return this.LIGHT_BROWN;
        } else {
            return this.BROWN;
        }
    } else if (str.includes("אפור") || str.includes("gray")) {
        if (str.includes("כה") || str.includes("dark")){
            return this.DARK_GRAY;
        } else if (str.includes("בהיר") || str.includes("light")) {
            return this.LIGHT_GRAY;
        } else {
            return this.GRAY;
        }
    } else {
        return this.WHITE;
    }
},
 BLUE : '4a66cc',
 LIGHT_BLUE : '90d6f6',
 DARK_BLUE : '34478c',
 PINK : 'f9a6a8',
 TOURQUOISE : '28efd1',
 GREEN : '2caf86',
 DARK_GREEN : '2b604f',
 LIGHT_GREEN : '68d7b4',
 RED : 'ff4242',
 BORDO : '892e30',
 WHITE : 'FFFFFF',
 BLACK : '000000',
 YELLOW : 'fcc505',
 PURPLE : '870ec0',
 LIGHT_PURPLE : 'c070e6',
 DARK_PURPLE : '55296a',
 ORANGE : 'ffa20f',
 BROWN : '9b6163',
 LIGHT_BROWN : '9c7e60',
 DARK_BROWN : '7b3235',
 GRAY : 'a6a7a7',
 DARK_GRAY : '536161',
 LIGHT_GRAY : 'cdd2d1',
 BROWN_HAIR : '553e0d',
 BLOND_HAIR : 'ffed58',
 GINGER_HAIR : 'f29010',
}