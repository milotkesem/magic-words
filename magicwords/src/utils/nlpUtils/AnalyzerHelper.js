export default {
seperateTokens : function(str) {
    var nouns = ["קוקו","עיניים","עינים","שיער","מכנסים","מכנסיים","חולצה","שמלה","כובע","נעלי","סנדלים","משקפיים","הולך","הולכת","מטיל","מטייל","לקרוא","פלייסטיישן","פליסטישן","פלייסטישן","פליסטיישן","לצייר","כדורסל","כדורגל","לגו","בלט","חוף","מגרש","רחוב","חדר","בית","גינה","אומרת","מספרת","שרה","הולכת","רצה",'"',"הולך","רץ","שירה", "אורי", "דנה", "דן", "דניאל", "מיטל", "מוריה","ליה", "מאיה", "אורן", "אלון","שער","משקפים","מסיבה","הופעה","כיתה","כתה","ענת","יעל","גיא","תומר","הילה","מתן","רועי","עומר","יובל","שר","בוכה","רוכב", "רוקד", "קורא", "כותב", "מתיישב", "יושב", "עומד", "נעמד"];
    var descArray = [];
    var wordArray = str.split(" ");
    var i = 0;
    var currentStr = "";
    for (i=0;i<wordArray.length;i++) {
        var word = wordArray[i];
        var isNoun = false;
        nouns.forEach(function(noun,index){
            if (word.includes(noun)) {
                isNoun = true;
            }         
        })
        
        if (!isNoun) {
            if (word.includes("בית") && wordArray[i+1].includes("ספר")) {
                isNoun = true;
                word = word + " " +  wordArray[i+1];
                i++;
            }
        }

        if (isNoun) {
            if (currentStr != "") {
                descArray.push(currentStr.substr(0,currentStr.length));
            }
            currentStr = word;
        } else {
            currentStr = currentStr + " " + word;
        }
    }
    descArray.push(currentStr.substr(0,currentStr.length));
    
    return descArray;
},

addChangeTodo : function(object, newAttachment, todo) {
    var currTodo = {"action":"change","object":object,"newAttachment":newAttachment, "timeout":500};
    todo.push(currTodo);
    return todo;
},
    
 addTintTodo : function(object, color, todo) {
    var currTodo = {"action":"tint","object":object,"color":color, "timeout":500};
    todo.push(currTodo);
    return todo;
},

 addAlphaTodo : function(object,alpha, todo) {
    var currTodo = {"action":"alpha","object":object,"alpha":alpha, "timeout":500};
    todo.push(currTodo);
    return todo;
},

addBackgroundTodo : function(backgroundName, todo) {
    var currTodo = {action:"background","fileName":"images/backgrounds/" + backgroundName + ".jpg", "timeout":500}; 
    todo.push(currTodo);
    return todo;
},

addAnimationTodo: function(animationName,duration,speed, todo, timeout) {
    var currTodo = {action:"animation","animationName":animationName,"duration":duration,"speed":speed,"timeout":timeout}; 
    todo.push(currTodo);
    return todo;
},

addSpeechTodo : function(speechText, todo) {
    var currTodo = {action:"speech","speechText":speechText, "timeout":3050}; 
    todo.push(currTodo);
    return todo;
},

addCharacterTodo : function(characterName, todo) {
    var currTodo = {action:"character","characterName":characterName, "timeout":500}; 
    todo.push(currTodo);
    return todo;
},

}