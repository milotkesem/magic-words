export default {
    "shirt-dress":[{
                        "slotName":"torso",
                        "slotIndex": -1,
                        "slotAttachments":{"shirt":"Boy/torso","dress":"girl/torso"}
                    },
                    {
                        "slotName":"hand top left",
                        "slotIndex": -1,
                        "slotAttachments":{"sleeve":"Boy/hand top left"}
                    },
                    {
                        "slotName":"hand top right",
                        "slotIndex": -1,
                        "slotAttachments":{"sleeve":"Boy/hand top right"}
                    }],
    "pants":[{
                "slotName":"pelvis",
                "slotIndex": -1,
                "slotAttachments":{"pelvis":"Boy/pelvis"}
            },
            {
                "slotName":"upper leg left",
                "slotIndex": -1,
                "slotAttachments":{"left pant":"Boy/upper leg left"}
            },
            {
                "slotName":"upper leg right",
                "slotIndex": -1,
                "slotAttachments":{"right pant":"Boy/upper leg right"}
            }],
    "short hair": [{
                    "slotName":"hair front",
                    "slotIndex": -1,
                    "slotAttachments":{"curly":"Boy/hair curly","straight":"Boy/hair straight", 
                                       "ponytail":"girl/Layer 9", "long prep":"girl/Layer 8"}
                   }],
    "shoes":[{
                "slotName":"right shoe wear",
                "slotIndex": -1,
                "slotAttachments":{"shoe":"Boy/shoe right","sandal":"girl/sandal", 
                                   "barefoot":"girl/empty"}
            },
        {
                "slotName":"left shoe wear",
                "slotIndex": -1,
                "slotAttachments":{"shoe":"Boy/shoe left","sandal":"girl/sandal", 
                                    "barefoot":"girl/empty"}
            }],
    "long hair":[{
                    "slotName":"hair back",
                    "slotIndex": -1,
                    "slotAttachments":{"none":"girl/empty","straight":"girl/Layer 5", 
                                       "curly":"girl/Layer 6"}
                }],
    "long sleeves":[{
                    "slotName":"long sleeve right",
                    "slotIndex": -1,
                    "slotAttachments":{"sleeve":"Boy/long sleeve right","none":"girl/empty"}
                },
                {
                    "slotName":"long sleeve left",
                    "slotIndex": -1,
                    "slotAttachments":{"sleeve":"Boy/long sleeve left","none":"girl/empty"}
                }],
    "long pants":[{
                    "slotName":"long pants right",
                    "slotIndex": -1,
                    "slotAttachments":{"pant":"Boy/long sleeve leg right","none":"girl/empty"}
                },
                {
                    "slotName":"long pants left",
                    "slotIndex": -1,
                    "slotAttachments":{"pant":"Boy/long sleeve leg left","none":"girl/empty"}
                }],
    "glasses":[{
                    "slotName":"Glasses",
                    "slotIndex": -1,
                    "slotAttachments":{"none":"girl/empty","glasses":"girl/glasses"}
                }],
    "hat":[{
                "slotName":"Hat",
                "slotIndex": -1,
                "slotAttachments":{"none":"girl/empty","hat":"girl/hat"}
            }],
    "eyes":[{
                "slotName":"eye right",
                "slotIndex": -1,
                "slotAttachments":{"black":"Boy/eye right","green":"girl/eye green right", 
                                    "blue":"girl/eye blue right","brown":"girl/eye brown right"}
            },
            {
                "slotName":"eye left",
                "slotIndex": -1,
                "slotAttachments":{"black":"Boy/eye left","green":"girl/eye green", 
                                    "blue":"girl/eye left blue","brown":"girl/eye left brown"}
            }],
    "ballet":[{
                "slotName":"ballet",
                "slotIndex": -1,
                "slotAttachments":{"none":"girl/empty","ballet":"ballet"}
            }],
    "lego":[{
                "slotName":"lego",
                "slotIndex": -1,
                "slotAttachments":{"none":"girl/empty","lego":"lego"}
            }],
    "basketball":[{
                "slotName":"basketball",
                "slotIndex": -1,
                "slotAttachments":{"none":"girl/empty","basketball":"basketball"}
            }],
    "football":[{
                "slotName":"football",
                "slotIndex": -1,
                "slotAttachments":{"none":"girl/empty","football":"football"}
            }],
    "playstation":[{
                "slotName":"playstation",
                "slotIndex": -1,
                "slotAttachments":{"none":"girl/empty","playstation":"playstation"}
            }],
    "paint":[{
                "slotName":"paint",
                "slotIndex": -1,
                "slotAttachments":{"none":"girl/empty","paint":"paint"}
            }],
    "book":[{
                "slotName":"book",
                "slotIndex": -1,
                "slotAttachments":{"none":"girl/empty","book":"book"}
            }]
};
