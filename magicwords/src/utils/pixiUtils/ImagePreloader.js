var imageFileNames = [
    "/images/backgrounds/park.jpg",
     "/images/backgrounds/beach.jpg",
     "/images/backgrounds/bedroom.jpg",
     "/images/backgrounds/soccer_field.jpg",
     "/images/backgrounds/street.jpg",   
 ]
 
var preLoadIndex = 0;
var stop = false;
var innerPixiLoader;

 export default {
     stopPreload : function() {
         stop = true;
     },
     preload : function(pixiLoader) {
         if (pixiLoader) {
            innerPixiLoader = pixiLoader;
         }
            if ((preLoadIndex < imageFileNames.length) && !stop) {
              var fileName = imageFileNames[preLoadIndex];
              preLoadIndex++;
              if (innerPixiLoader.resources[fileName] == undefined) {
                  innerPixiLoader.add(fileName).load(this.preload.bind(this));
              } else {
                this.preload();
              }
            }
     }
 }